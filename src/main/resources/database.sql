-- Active: 1709546324780@@127.0.0.1@3306@tenta_douce
INSERT INTO user (pseudo, email, password, role) VALUES
("Admin", "admin@test.com", "$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq", "ROLE_ADMIN"),
("User","user@test.com", "$2a$12$Iecy.0VI3Q2A8LeFgd32cOqB3WbY6otlQ.7UQMV9O2cHDNO9dMJBy", "ROLE_USER");

INSERT INTO categorie (nom) VALUES
('Macaron'),
('Chocolat'),
('Petits Gateaux'),
('Janponais');
