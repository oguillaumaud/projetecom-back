package co.simplon.doucetentations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DouceTentationsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DouceTentationsApplication.class, args);
	}

}
