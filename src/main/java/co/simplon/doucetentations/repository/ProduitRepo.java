package co.simplon.doucetentations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.simplon.doucetentations.entities.Produit;

public interface ProduitRepo extends JpaRepository<Produit, Integer> {

}
