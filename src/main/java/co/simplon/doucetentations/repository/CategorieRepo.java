package co.simplon.doucetentations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.simplon.doucetentations.entities.Categorie;

public interface CategorieRepo extends JpaRepository<Categorie, Integer>{

}
