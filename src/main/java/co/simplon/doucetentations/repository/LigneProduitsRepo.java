package co.simplon.doucetentations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.simplon.doucetentations.entities.LigneProduits;

public interface LigneProduitsRepo extends JpaRepository<LigneProduits, Integer>{

}
