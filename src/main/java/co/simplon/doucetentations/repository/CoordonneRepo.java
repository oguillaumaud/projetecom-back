package co.simplon.doucetentations.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import co.simplon.doucetentations.entities.Coordonne;

public interface CoordonneRepo extends JpaRepository<Coordonne, Integer>{

    @Query("select c from Coordonne c where c.author.id=?1")
    List<Coordonne> findByAuthor(int idUser);
    
}
