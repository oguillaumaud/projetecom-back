package co.simplon.doucetentations.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.doucetentations.entities.Image;

@Repository
public interface ImageRepo extends JpaRepository<Image, Integer>{

}
