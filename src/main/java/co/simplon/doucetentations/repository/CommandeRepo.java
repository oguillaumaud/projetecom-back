package co.simplon.doucetentations.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import co.simplon.doucetentations.entities.Commande;

public interface CommandeRepo extends JpaRepository<Commande, Integer> {

    @Query("select co from Commande co where co.author.id=?1")
    Page<Commande> findByUser(int idAuther, Pageable pageable);
}
