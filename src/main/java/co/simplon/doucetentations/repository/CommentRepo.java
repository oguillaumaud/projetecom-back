package co.simplon.doucetentations.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import co.simplon.doucetentations.entities.Comment;

@Repository
public interface CommentRepo extends JpaRepository<Comment, Integer>{

    @Query("select c from Comment c where c.author.id=?1")
    Page<Comment> findByUser(int idAuther, Pageable pageable);

    List<Comment> findAllByProduitId(int produitId);

}
