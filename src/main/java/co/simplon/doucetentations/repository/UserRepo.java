package co.simplon.doucetentations.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import co.simplon.doucetentations.entities.User;

public interface UserRepo extends JpaRepository<User, Integer> {
    Optional<User> findByEmail(String email);

    void deleteByEmail(String email);

}
