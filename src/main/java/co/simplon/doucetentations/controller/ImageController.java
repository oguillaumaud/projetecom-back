package co.simplon.doucetentations.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.doucetentations.entities.Image;
import co.simplon.doucetentations.entities.User;
import co.simplon.doucetentations.repository.ImageRepo;
import jakarta.validation.Valid;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/api/image")
public class ImageController {

    @Autowired
    private ImageRepo repo;

    @GetMapping
    public List<Image> findAll() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Image one(@Valid @PathVariable int id) {
        return repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public ResponseEntity<Image> add(@Valid @RequestBody Image image) {
        Image savedImage = repo.save(image);
        return ResponseEntity.ok(savedImage);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable int id) {
        Image image = one(id);
        if (image == null) {
            return ResponseEntity.notFound().build(); 
        }
        repo.delete(image);
        return ResponseEntity.ok().build();
    }
    

    @PutMapping("/{id}")
    public Image update(@PathVariable int id, @Valid @RequestBody Image image, @AuthenticationPrincipal User user) {
        Image imgUpdate = one(id);
        imgUpdate.setSrc(image.getSrc());
        imgUpdate.setDescription(image.getDescription());
        imgUpdate.setProduit(image.getProduit());
        repo.save(imgUpdate);
        return imgUpdate;
    }

    @PostMapping("/upload")
    public String upload(@RequestParam MultipartFile image) {
        String rename = UUID.randomUUID() + ".jpg";
        try {
            Thumbnails.of(image.getInputStream())
                    .width(1000)
                    .toFile(new File(getUploadFolder(), rename));
            Thumbnails.of(image.getInputStream())
                    .size(300, 300)
                    .crop(Positions.CENTER)
                    .toFile(new File(getUploadFolder(), "thumbnail-" + rename));
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Upload error", e);
        }
        return rename;
    }

    private File getUploadFolder() {
        File folder = new File(getClass().getClassLoader().getResource(".").getPath().concat("static/uploads"));
        if (!folder.exists()) {
            folder.mkdirs();
        }
        return folder;
    }

}
