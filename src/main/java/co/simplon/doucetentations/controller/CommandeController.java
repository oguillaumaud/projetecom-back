package co.simplon.doucetentations.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.doucetentations.entities.Commande;
import co.simplon.doucetentations.entities.User;
import co.simplon.doucetentations.repository.CommandeRepo;

@RestController
@RequestMapping("/api/commande")
public class CommandeController {
    @Autowired
    private CommandeRepo repo;

    @GetMapping
    public List<Commande> all() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Commande findById(@PathVariable Integer id) {
        return repo.findById(id).get();
    }

    @PostMapping()
    public Commande postCommande( @RequestBody Commande Commande) {
        return repo.save(Commande);
    }
    
    @PostMapping("/update")
    public Commande updateCommande( @RequestBody Commande Commande) {
        Commande newPic = findById(Commande.getId());
        return repo.save(newPic);
    }
    
    @DeleteMapping("/{id}")
    public void deleteCommande(@PathVariable Integer id){
        Commande pic = findById(id);
        repo.delete(pic);
    }

        @GetMapping("/user/{idAuther}")
    public Page<Commande> getUserCommande(@AuthenticationPrincipal User user, @PathVariable int idAuther,
                                        @RequestParam(defaultValue = "0") int pageNumber, 
                                        @RequestParam(defaultValue = "10") int pageSize) {
        if(pageSize > 30){
            pageSize = 30;
        }
        Pageable page = Pageable.ofSize(pageSize).withPage(pageNumber);
        return repo.findByUser(idAuther, page);
    }
}
