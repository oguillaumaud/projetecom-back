package co.simplon.doucetentations.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.doucetentations.entities.Coordonne;
import co.simplon.doucetentations.entities.User;
import co.simplon.doucetentations.repository.CoordonneRepo;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/coordonne")
public class CoordonneController {
    @Autowired
    private CoordonneRepo repo;

    @GetMapping
    public List<Coordonne> all() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Coordonne findById(@PathVariable Integer id) {
        return repo.findById(id).get();
    }

    @GetMapping("/user/{idUser}")
    public List<Coordonne> byUser(@AuthenticationPrincipal User user, @Valid @PathVariable int idUser) {
        return repo.findByAuthor(idUser);
    }

    @PostMapping()
    public Coordonne postCoordonne(@Valid @RequestBody Coordonne coordonne, @AuthenticationPrincipal User user) {
        coordonne.setAuthor(user);
        return repo.save(coordonne);
    }

    @PostMapping("/update")
    public Coordonne updateCoordonne(@RequestBody Coordonne updatedCoordonne) {
        Coordonne existingCoordonne = findById(updatedCoordonne.getId());

        existingCoordonne.setNom(updatedCoordonne.getNom());
        existingCoordonne.setPrenom(updatedCoordonne.getPrenom());
        existingCoordonne.setAdress(updatedCoordonne.getAdress());
        existingCoordonne.setComplement(updatedCoordonne.getComplement());
        existingCoordonne.setCodePostal(updatedCoordonne.getCodePostal());
        existingCoordonne.setVille(updatedCoordonne.getVille());
        existingCoordonne.setPays(updatedCoordonne.getPays());
        existingCoordonne.setTelephone(updatedCoordonne.getTelephone());

        return repo.save(existingCoordonne);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCoordonne(@PathVariable Integer id) {
        Coordonne coordonne = findById(id);
        repo.delete(coordonne);
    }

}
