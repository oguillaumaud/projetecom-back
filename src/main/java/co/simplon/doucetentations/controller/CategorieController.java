package co.simplon.doucetentations.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.doucetentations.entities.Categorie;
import co.simplon.doucetentations.repository.CategorieRepo;

@RestController
@RequestMapping("/api/categorie")
public class CategorieController {
    @Autowired
    private CategorieRepo repo;

    @GetMapping
    public List<Categorie> all() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Categorie findById(@PathVariable Integer id) {
        return repo.findById(id).get();
    }

    @PostMapping()
    public Categorie postCategorie( @RequestBody Categorie Categorie) {
        return repo.save(Categorie);
    }
    
    @PostMapping("/update")
    public Categorie updateCategorie( @RequestBody Categorie categorie) {
        Categorie newPic = findById(categorie.getId());
        newPic.setNom(categorie.getNom());
        return repo.save(newPic);
    }
    
    @DeleteMapping("/{id}")
    public void deleteCategorie(@PathVariable Integer id){
        Categorie pic = findById(id);
        repo.delete(pic);
    }
}
