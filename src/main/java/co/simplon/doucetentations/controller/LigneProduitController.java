package co.simplon.doucetentations.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.doucetentations.entities.LigneProduits;
import co.simplon.doucetentations.repository.LigneProduitsRepo;

@RestController
@RequestMapping("/api/ligne-produits")
public class LigneProduitController {
    @Autowired
    private LigneProduitsRepo repo;

    @GetMapping
    public List<LigneProduits> all() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public LigneProduits findById(@PathVariable Integer id) {
        return repo.findById(id).get();
    }

    @PostMapping()
    public LigneProduits postLigneProduits( @RequestBody LigneProduits LigneProduits) {
        return repo.save(LigneProduits);
    }
    
    @PostMapping("/update")
    public LigneProduits updateLigneProduits( @RequestBody LigneProduits LigneProduits) {
        LigneProduits newPic = findById(LigneProduits.getId());
        newPic.setQuantite(LigneProduits.getQuantite());
        newPic.setPrix(LigneProduits.getPrix());
        return repo.save(newPic);
    }
    
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteLigneProduits(@PathVariable Integer id){
        LigneProduits pic = findById(id);
        repo.delete(pic);
    }
}
