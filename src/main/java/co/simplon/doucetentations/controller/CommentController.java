package co.simplon.doucetentations.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.doucetentations.entities.Comment;
import co.simplon.doucetentations.entities.User;
import co.simplon.doucetentations.repository.CommentRepo;
import jakarta.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@RestController
@RequestMapping("/api/comment")
public class CommentController {

    @Autowired
    private CommentRepo repo;


    @GetMapping
    public List<Comment> all() {
        return repo.findAll();
    }
    
    @GetMapping("/{id}")
    public Comment one(@PathVariable int id) {
        return repo.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Comment add(@Valid @RequestBody Comment comment, @AuthenticationPrincipal User user) {
        comment.setDate(LocalDate.now());
        repo.save(comment);
        System.out.println(comment);
        return comment;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id, @AuthenticationPrincipal User user) {
        Comment deleted = one(id);
        if (deleted.getAuthor() == null || !deleted.getAuthor().getId().equals(user.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Not your comment");
        }
        repo.delete(deleted);
    }

    @PostMapping("/{id}")
    public Comment update(@PathVariable int id, @Valid @RequestBody Comment comment, @AuthenticationPrincipal User user) {
        Comment toUpdate = one(id);

        if (toUpdate.getAuthor() == null || !toUpdate.getAuthor().getId().equals(user.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Not your comment");
        }
        toUpdate.setText(comment.getText());
        toUpdate.setNote(comment.getNote());
        toUpdate.setDate(LocalDate.now());
        toUpdate.setProduit(comment.getProduit());
        repo.save(toUpdate);
        return toUpdate;
    }

    @GetMapping("/user/{idAuther}")
    public Page<Comment> getUserComment(@AuthenticationPrincipal User user, @PathVariable int idAuther,
                                        @RequestParam(defaultValue = "0") int pageNumber, 
                                        @RequestParam(defaultValue = "10") int pageSize) {
        if(pageSize > 30){
            pageSize = 30;
        }
        Pageable page = Pageable.ofSize(pageSize).withPage(pageNumber);
        return repo.findByUser(idAuther, page);
    }

    @GetMapping("/produit/{produitId}")
    public List<Comment> getCommentsByProduit(@PathVariable int produitId) {
        List<Comment> comments = repo.findAllByProduitId(produitId);
        comments = comments.reversed();
        return comments;
    }

    @GetMapping("/produit/notebyproduit/{produitId}")
    public Integer getCommentsByProduitNoteMoyenne(@PathVariable int produitId) {
        List<Comment> comments = repo.findAllByProduitId(produitId);
        int note = 0;
        for (Comment comment : comments) {
            note += comment.getNote();
        }
        return note / comments.size();
    }

}
