package co.simplon.doucetentations.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.doucetentations.entities.Image;
import co.simplon.doucetentations.entities.Produit;
import co.simplon.doucetentations.entities.User;
import co.simplon.doucetentations.repository.ImageRepo;
import co.simplon.doucetentations.repository.ProduitRepo;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/produit")
public class ProduitController {
    @Autowired
    private ProduitRepo repo;

    @Autowired
    private ImageRepo repoImage;

    @GetMapping
    public List<Produit> all() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Produit findById(@PathVariable Integer id) {
        return repo.findById(id).get();
    }

    @GetMapping("/last")
    public Produit findLastProduit() {
        List<Produit> all = repo.findAll();
        if (all.isEmpty()) {
            return null;
        }

        return all.getLast();
    }

    @PostMapping()
    public Produit postProduit(@RequestBody Produit produit) {
        for (Image image : produit.getImages()) {
            image = repoImage.save(image);
            image.setProduit(produit);
        }
        return repo.save(produit);

    }

    
    @PutMapping("/{id}")
    public Produit updateProduit(@PathVariable int id, @Valid @RequestBody Produit Produit,
            @AuthenticationPrincipal User user) {
        Produit newPic = findById(id);
        newPic.setTitre(Produit.getTitre());
        newPic.setDescription(Produit.getDescription());
        newPic.setPrix(Produit.getPrix());
        newPic.setCategorie(Produit.getCategorie());
        return repo.save(newPic);
    }

    @DeleteMapping("/{id}")
    public void deleteProduit(@PathVariable Integer id) {
        Produit pic = findById(id);
        repo.delete(pic);
    }
}
