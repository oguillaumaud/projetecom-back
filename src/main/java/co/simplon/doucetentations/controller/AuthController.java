package co.simplon.doucetentations.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.doucetentations.entities.Comment;
import co.simplon.doucetentations.entities.User;
import co.simplon.doucetentations.repository.UserRepo;
import jakarta.validation.Valid;


@RestController
public class AuthController {

    @Autowired
    private PasswordEncoder hasher;
    @Autowired
    private UserRepo repo;



    @PostMapping("/api/user")
    @ResponseStatus(HttpStatus.CREATED)
    public User register(@Valid @RequestBody User user) {
        if(repo.findByEmail(user.getEmail()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
        }

        String hash = hasher.encode(user.getPassword());
        user.setPassword(hash);
        user.setRole("ROLE_USER");
        repo.save(user);
        return user;
    }

    @GetMapping("/api/account")
    public User myAccount(@AuthenticationPrincipal User user) {
        return user;
    }

}
