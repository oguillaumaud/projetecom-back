package co.simplon.doucetentations.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;

@Entity
public class Commande {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    private Integer id;

    @NotBlank
    @ManyToOne
    private User author;

    @NotBlank
    private Integer prix;

    @JsonIgnore
    @OneToMany(mappedBy = "commandes")
    private List<LigneProduits> ligneProduits;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Integer getPrix() {
        return prix;
    }

    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public List<LigneProduits> getLigneProduits() {
        return ligneProduits;
    }

    public void setLigneProduits(List<LigneProduits> ligneProduits) {
        this.ligneProduits = ligneProduits;
    }

}
