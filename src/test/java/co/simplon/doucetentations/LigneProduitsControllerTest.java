package co.simplon.doucetentations;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class LigneProduitsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetAllLigneProduits() throws Exception {
        mockMvc.perform(get("/api/ligne-produits"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void testGetLigneProduitsById() throws Exception {
        mockMvc.perform(get("/api/ligne-produits/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(2));
    }

    @Test
    @Rollback
    @WithUserDetails("admin@test.com")
    public void testCreateLigneProduits() throws Exception {
        String requestBody = "{\"quantite\": 10, \"prix\": 50}";

        mockMvc.perform(post("/api/ligne-produits")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.quantite").value(10))
                .andExpect(jsonPath("$.prix").value(50));
    }

    @Test
    @Rollback
    @WithUserDetails("admin@test.com")
    public void testDeleteLigneProduits() throws Exception {
        mockMvc.perform(delete("/api/ligne-produits/2"))
                .andExpect(status().isNoContent());
    }
}
