package co.simplon.doucetentations;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class CommentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetAllComments() throws Exception {
        mockMvc.perform(get("/api/comment"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void testGetCommentById() throws Exception {
        mockMvc.perform(get("/api/comment/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(2));
    }

    @Test
    @Rollback
    @WithUserDetails("admin@test.com")
    public void testCreateComment() throws Exception {
        String requestBody = "{\"text\": \"Great product!\", \"note\": 5, \"produit\":  {\n" + //
                        "    \"id\": 2,\n" + //
                        "    \"titre\": \"a\",\n" + //
                        "    \"description\": \"a\",\n" + //
                        "    \"prix\": 1,\n" + //
                        "    \"categorie\": null,\n" + //
                        "    \"ligneProduits\": null\n" + //
                        "  }}";

        mockMvc.perform(post("/api/comment")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.text").value("Great product!"))
                .andExpect(jsonPath("$.note").value(5));
    }


@Test
@Rollback
@WithUserDetails("admin@test.com")
public void testDeleteComment() throws Exception {
    mockMvc.perform(delete("/api/comment/2"))
            .andExpect(status().isNoContent());
}

@Test
@Rollback
@WithUserDetails("admin@test.com")
public void testUpdateComment() throws Exception {
    String requestBody = "  {\n" + //
                "    \"id\": 2,\n" + //
                "    \"text\": \"Updated comment\",\n" + //
                "    \"note\": 4,\n" + //
                "    \"date\": \"2024-05-08\",\n" + //
                "    \"produit\": null,\n" + //
                "    \"author\": {\n" + //
                "      \"id\": 2,\n" + //
                "      \"email\": \"admin@test.com\",\n" + //
                "      \"role\": \"ROLE_ADMIN\",\n" + //
                "      \"pseudo\": \"admin\",\n" + //
                "      \"username\": \"admin@test.com\",\n" + //
                "      \"authorities\": [\n" + //
                "        {\n" + //
                "          \"authority\": \"ROLE_ADMIN\"\n" + //
                "        }\n" + //
                "      ],\n" + //
                "      \"accountNonExpired\": true,\n" + //
                "      \"accountNonLocked\": true,\n" + //
                "      \"credentialsNonExpired\": true,\n" + //
                "      \"enabled\": true\n" + //
                "    }\n" + //
                "  }";

    mockMvc.perform(post("/api/comment/2")
            .contentType(MediaType.APPLICATION_JSON)
            .content(requestBody))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.text").value("Updated comment"))
            .andExpect(jsonPath("$.note").value(4));
}
}
