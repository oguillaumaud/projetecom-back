package co.simplon.doucetentations;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class CoordonneControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetAllCoordonne() throws Exception {
        mockMvc.perform(get("/api/coordonne"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void testGetCoordonneById() throws Exception {
        mockMvc.perform(get("/api/coordonne/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1));
    }

    @Test
    @Rollback
    @WithUserDetails("admin@test.com")
    public void testCreateCoordonne() throws Exception {
        String requestBody = "{\"nom\": \"Doe\", \"prenom\": \"John\", \"adress\": \"123 Rue de la Ville\", \"codePostal\": 12345, \"ville\": \"Ville\", \"pays\": \"France\", \"telephone\": \"0123456789\"}";

        mockMvc.perform(post("/api/coordonne")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nom").value("Doe"))
                .andExpect(jsonPath("$.prenom").value("John"));
    }

    @Test
    @Rollback
    @WithUserDetails("admin@test.com")
    public void testDeleteCoordonne() throws Exception {
        mockMvc.perform(delete("/api/coordonne/1"))
                .andExpect(status().isNoContent());
    }

    @Test
    @Rollback
    @WithUserDetails("admin@test.com")
    public void testUpdateCoordonne() throws Exception {
        String requestBody = "{\"id\": 1, \"nom\": \"Updated Nom\", \"prenom\": \"Updated Prenom\", \"adress\": \"Updated Adresse\", \"codePostal\": 54321, \"ville\": \"Updated Ville\", \"pays\": \"Updated Pays\", \"telephone\": \"9876543210\"}";
        mockMvc.perform(post("/api/coordonne/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nom").value("Updated Nom"))
                .andExpect(jsonPath("$.prenom").value("Updated Prenom"));
    }
}
