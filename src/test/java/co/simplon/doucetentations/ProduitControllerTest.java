package co.simplon.doucetentations;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class ProduitControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetAllProduits() throws Exception {
        mockMvc.perform(get("/api/produit"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void testGetProduitById() throws Exception {
        mockMvc.perform(get("/api/produit/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1));
    }

    @Test
    @Rollback
    @WithMockUser(username = "admin", roles = { "USER", "ADMIN" })
    public void testCreateProduit() throws Exception {
        String requestBody = "{\"titre\": \"Nouveau produit\", \"description\": \"Description du nouveau produit\", \"prix\": 100}";

        mockMvc.perform(post("/api/produit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.titre").value("Nouveau produit"))
                .andExpect(jsonPath("$.description").value("Description du nouveau produit"))
                .andExpect(jsonPath("$.prix").value(100));
    }

    @Test
    @Rollback
    @WithMockUser(username = "admin", roles = { "USER", "ADMIN" })
    public void testUpdateProduit() throws Exception {
        String requestBody = "{\"titre\": \"Produit mis à jour\", \"description\": \"Description mise à jour\", \"prix\": 150}";

        mockMvc.perform(put("/api/produit/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.titre").value("Produit mis à jour"))
                .andExpect(jsonPath("$.description").value("Description mise à jour"))
                .andExpect(jsonPath("$.prix").value(150));
    }

    @Test
    @Rollback
    @WithMockUser(username = "admin", roles = { "USER", "ADMIN" })
    public void testDeleteProduit() throws Exception {
        mockMvc.perform(delete("/api/produit/1"))
                .andExpect(status().isOk());
    }
}
