package co.simplon.doucetentations;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc

public class CommandeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetAllCommandes() throws Exception {
        mockMvc.perform(get("/api/commande"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void testGetCommandeById() throws Exception {
        mockMvc.perform(get("/api/commande/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1));
    }

    @Test
    @WithUserDetails("admin@test.com")
    public void testCreateCommande() throws Exception {
        String requestBody = "{\"author\": {\"id\": 1}, \"prix\": 100}";

        mockMvc.perform(post("/api/commande")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.prix").value(100));
    }

    @Test
    @WithUserDetails("admin@test.com")
    public void testDeleteCommande() throws Exception {
        mockMvc.perform(delete("/api/commande/1"))
                .andExpect(status().isOk());
    }
}
