package co.simplon.doucetentations;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import co.simplon.doucetentations.entities.Image;
import co.simplon.doucetentations.entities.Produit;
import co.simplon.doucetentations.repository.ImageRepo;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
public class ImageControllerTest {

        @Autowired
        private MockMvc mockMvc;

        @MockBean
        private ImageRepo imageRepo;

        @Test
        public void testGetAllImages() throws Exception {
                Image image1 = new Image();
                Image image2 = new Image();
                List<Image> images = Arrays.asList(image1, image2);

                when(imageRepo.findAll()).thenReturn(images);

                mockMvc.perform(get("/api/image"))
                                .andExpect(status().isOk())
                                .andExpect(jsonPath("$").isArray())
                                .andExpect(jsonPath("$[0].id").value(image1.getId()))
                                .andExpect(jsonPath("$[1].id").value(image2.getId()));
        }

        @Test
        public void testGetImageById() throws Exception {
                Image image = new Image();
                image.setId(1);

                when(imageRepo.findById(1)).thenReturn(Optional.of(image));

                mockMvc.perform(get("/api/image/1"))
                                .andExpect(status().isOk())
                                .andExpect(jsonPath("$.id").value(image.getId()));
        }

        @Test
        @WithUserDetails("admin@test.com")
        public void testAddImage() throws Exception {
                Image image = new Image();
                image.setId(1);
                image.setSrc("test.jpg");
                image.setDescription("test");
                Produit produit = new Produit();
                produit.setId(2);
                produit.setTitre("a");
                produit.setDescription("a");
                produit.setPrix(1);
                image.setProduit(produit);

                when(imageRepo.save(any(Image.class))).thenReturn(image);

                mockMvc.perform(post("/api/image")
                                .contentType("application/json")
                                .content("{\n" +
                                                "  \"src\": \"test.jpg\",\n" +
                                                "  \"description\": \"test\",\n" +
                                                "  \"produit\": {\n" +
                                                "    \"id\": 2,\n" +
                                                "    \"titre\": \"a\",\n" +
                                                "    \"description\": \"a\",\n" +
                                                "    \"prix\": 1,\n" +
                                                "    \"categorie\": null,\n" +
                                                "    \"ligneProduits\": null\n" +
                                                "  }\n" +
                                                "}"))
                                .andExpect(status().isOk())
                                .andExpect(jsonPath("$.id").isNotEmpty());
        }

        @Test
        @WithUserDetails("admin@test.com")
        public void testDeleteImage() throws Exception {
                Image imageWithId4 = new Image();
                imageWithId4.setId(4);

                when(imageRepo.findById(4)).thenReturn(Optional.of(imageWithId4));
                when(imageRepo.findById(5)).thenReturn(Optional.empty());

                Optional<Image> imageOptionalBeforeDelete = imageRepo.findById(4);
                assertTrue(imageOptionalBeforeDelete.isPresent());

                mockMvc.perform(delete("/api/image/4"))
                                .andExpect(status().isOk());

                mockMvc.perform(delete("/api/image/5"))
                                .andExpect(status().isNotFound());
        }

}
