package co.simplon.doucetentations;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import jakarta.transaction.Transactional;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class AuthControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    @WithUserDetails("admin@test.com")
    public void testRegisterUser() throws Exception {
        String requestBody = "{\"email\": \"test1@test.com\", \"password\": \"password\"}";

        mockMvc.perform(post("/api/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.email").value("test1@test.com"));
    }

    @Test
    @WithUserDetails("admin@test.com")
    public void testRegisterExistingUser() throws Exception {
        String requestBody = "{\"email\": \"test@test.com\", \"password\": \"1234\"}";

        mockMvc.perform(post("/api/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithUserDetails("admin@test.com")
    public void testRegisterUserWithShortPassword() throws Exception {
        String requestBody = "{\"email\": \"test@test.com\", \"password\": \"123\"}";

        mockMvc.perform(post("/api/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithUserDetails("admin@test.com")
    public void testRegisterUserWithInvalidEmail() throws Exception {
        String requestBody = "{\"email\": \"invalidemail\", \"password\": \"password\"}";

        mockMvc.perform(post("/api/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isBadRequest());
    }

}
